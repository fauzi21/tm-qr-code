# TM QR Code

A Flutter Application that generate QR Code with result as contact or address book, and this apps
can run on Android and iOS devices.

## Plugin
- [get](https://pub.dev/packages/get), it help developer to manage state or navigation manager more easier
- [get_storage](https://pub.dev/packages/get_storage), simple and fast for storing data in memory
- [qr_flutter](https://pub.dev/packages/qr_flutter), a library for simply and fast rendering QR Code via widget
- [flutter_contacts](https://pub.dev/packages/flutter_contacts), Flutter plugin to read, create, update, delete and observe native contacts on Android and iOS, with group support, vCard support, and contact permission handling.

## How to build archive ipa
1. Open Xcode and select archive from menu Product
2. After process archive done, select `Development` method of distribution
3. Check all Additional options `Rebuild from Bitcode`, `Strip Swift Symbols` and `Include manifest for over-the-air-installation`
4. On the Distribution manifest information set:<br/>
  - App URL : https://api-agent.tokiomarine-life.co.id/media/qrcode-sam/tm-qrcode.ipa<br/>
  - Display Image URL : https://api-agent.tokiomarine-life.co.id/media/icon57.png<br/>
  - Full Size Image URL : https://api-agent.tokiomarine-life.co.id/media/icon512.png
  ![picture](assets/ss_manifest.png)
5. Clik Next until process distribution done and export to ipa file

## How to share apk and ipa
Sharing file apk till now usually use chat room like Whatsapp or teams but for sharing file ipa will use apps [Cyberduck](https://cyberduck.io/download/)<br/>
1. Download and install apps Cyberduck on your laptop<br/>
2. Clik open connection and set like picture below
![picture](assets/ss_login.png)
3. Upload .ipa and manifest.plist to folder `/var/www/ap/ap/media_root/qrcode_sam`

## How to use
1. Install the application from .apk file or install this apps if it already published on playstore and apps store
2. For new user, TM QR Code will directly open profile page so user can update profile and save it on local storage.
![picture](assets/ss_profile.png)
3. After profile updated, TM QR Code will close the profile page and go back to main page
5. Main Page of TM QR Code will show generated QR Code with data from profile. There is button setting on below right of screen if you want to edit profile.
![picture](assets/ss_main_page.png)
