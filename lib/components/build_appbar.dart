import 'package:flutter/material.dart';
import 'package:tm_qrcode/base/base_color.dart';

AppBar BuildAppBar(String title) {

  return AppBar(
    title: Text(title),
    backgroundColor: mainColor,
    elevation: 0,
  );

}
