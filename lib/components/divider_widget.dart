import 'package:flutter/material.dart';

class DividerWidget extends StatelessWidget {
  const DividerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 8,
      color: Colors.grey.shade300,
      margin: EdgeInsets.only(
        bottom: 12
      ),
    );
  }
}
