import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tm_qrcode/base/base_size.dart';

class BuildDefaultTextfield extends StatelessWidget {

  final Function(String)? onChanged;
  final String? Function(String?)? validator;
  final String lableText;
  final bool isEnabled;
  final AutovalidateMode? autovalidateMode;
  final String? value;
  final String? prefix;
  final TextInputType textInputType;
  final TextEditingController? editingController;
  final EdgeInsets? margin;
  final bool isRequired;
  final TextCapitalization? capitalization;

  BuildDefaultTextfield({Key? key,
    this.onChanged,
    required this.lableText,
    this.validator,
    this.autovalidateMode,
    this.editingController,
    this.margin,
    this.value,
    this.prefix,
    this.isEnabled: true,
    this.textInputType: TextInputType.text,
    this.isRequired = true,
    this.capitalization
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
            left: 12,
            bottom: 6
          ),
          child: Text(lableText,
            style: TextStyle(
              fontSize: 12,
              color: Colors.black87,
              fontWeight: FontWeight.w400
            ),
          ),
        ),

        Container(
          margin: margin ?? EdgeInsets.only(
              bottom: 12
          ),
          decoration: BoxDecoration(
            color: Colors.grey.shade100,
            borderRadius: BorderRadius.circular(borderSizeDefault)
          ),
          child: TextFormField(
            controller: value != null
                ? TextEditingController(text: value)
                : editingController ?? TextEditingController(),
            keyboardType: textInputType,
            textCapitalization: capitalization ?? TextCapitalization.none,
            inputFormatters: [
              if (textInputType == TextInputType.number)
                FilteringTextInputFormatter.deny(RegExp(r'[^\d]'))
            ],
            maxLines: null,
            decoration: InputDecoration(
              enabled: isEnabled,
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              hintText: lableText,
              prefixText: prefix,
              contentPadding: EdgeInsets.all(12),
              isDense: true,
            ),
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: isEnabled ? Colors.black87 : Colors.black26
            ),
            onChanged: onChanged,
            validator:  (value) {
              if (isRequired) {
                if (value == null || value.isEmpty) {
                  return '$lableText cannot be empty';
                }
              }

              final _validator = this.validator;
              if (_validator != null) {
                return _validator(value);
              }
            },
            autovalidateMode: autovalidateMode,
          ),
        )
      ],
    );
  }
}
