import 'package:flutter/material.dart';
import 'package:tm_qrcode/base/base_size.dart';

class BuildDefaultButton extends StatelessWidget {

  final Function()? onPressed;
  final String lable;
  final Color? textColor, buttonColor;
  final double elevation;
  final double? cornerRadius;

  BuildDefaultButton({Key? key,
    this.onPressed,
    required this.lable,
    this.textColor,
    this.buttonColor,
    this.cornerRadius,
    required this.elevation
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      color: buttonColor,
      elevation: elevation,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(cornerRadius ?? borderSizeDefault),
        side:  BorderSide(
          color: Colors.grey,
          width: buttonColor != null ? 0 : 1,
        )
      ),
      child: Text(lable,
        style: TextStyle(
          fontSize: 12,
          color: textColor
        ),
      ),
    );
  }
}
