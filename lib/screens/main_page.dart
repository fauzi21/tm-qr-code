part of 'main_state.dart';

class MainPage extends StatefulWidget {
  MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends MainState<MainPage> {

  @override
  void initState() {
    /// run after build
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      checkIsHasUpdate();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: BuildAppBar('TM QRcode'),
      backgroundColor: mainColor,
      body: MainView(
        data: rawDataContact != null ? vCard() : '',
        onEdit: gotoProfile,
      ),
    );
  }




}
