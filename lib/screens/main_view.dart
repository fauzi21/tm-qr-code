part of 'main_state.dart';

class MainView extends StatelessWidget {

  final String data;
  final Function() onEdit;

  const MainView({Key? key,
    required this.data,
    required this.onEdit
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Stack(
      alignment: Alignment.center,
      children: [

        Positioned(
          top: 0,
          child: Container(
            margin: EdgeInsets.only(
              top: 12
            ),
            padding: EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(borderSizeDefault),
              color: Colors.white
            ),
            width: 100,
            height: 100,
            child: Image.asset('assets/tmli.jpeg',),
          ),
        ),

        Card(
          elevation: 2,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderSizeDefault)
          ),
          child: Padding(
            padding: EdgeInsets.all(24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                data.isEmpty
                    ? Container(
                  width: double.infinity,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(12),
                  color: Colors.grey[200],
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        child: Icon(Icons.close,
                          color: Colors.red,
                          size: 100,
                        ),
                        margin: EdgeInsets.only(
                            bottom: 6
                        ),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: Colors.red,
                                width: 6
                            )
                        ),
                      ),

                      Text('Your Profile data is empty!',
                        style: TextStyle(
                            fontSize: 14
                        ),
                      )
                    ],
                  ),
                ) : Column(
                  children: [
                    Container(
                      child: Text('Scan QR Code',
                        style: TextStyle(
                          fontSize: 18
                        ),
                      ),
                      margin: EdgeInsets.only(
                        bottom: 14
                      ),
                    ),
                    CustomPaint(
                      painter: BorderPainter(),
                      child: Container(
                        margin: EdgeInsets.all(12),
                        child: QrImage(
                          data: data,
                          embeddedImage: AssetImage('assets/tmli_logo.png'),
                          size: 250,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),

        Container(
          width: double.infinity,
          alignment: Alignment.topRight,
          margin: EdgeInsets.only(
            right: 24
          ),
          child: IconButton(
            key: Key(BaseKey.btnSetting),
            icon: Icon(Icons.settings,
              color: Colors.grey[700],
            ),
            onPressed: onEdit,
            iconSize: 32,
          ),
        )
      ],
    );
  }
}
