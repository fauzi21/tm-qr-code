import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_contacts/contact.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:flutter_contacts/properties/email.dart';
import 'package:flutter_contacts/properties/phone.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:tm_qrcode/base/base_color.dart';
import 'package:tm_qrcode/base/base_key.dart';
import 'package:tm_qrcode/base/base_size.dart';
import 'package:tm_qrcode/base/base_string.dart';
import 'package:tm_qrcode/components/border_painter.dart';
import 'package:tm_qrcode/components/build_appbar.dart';
import 'package:tm_qrcode/screens/profile/profile_state.dart';

part 'main_page.dart';
part 'main_view.dart';

abstract class MainState<T extends StatefulWidget> extends State<T> {
  final storage = GetStorage();

  Map<String, dynamic>? rawDataContact;
  String? firstName, lastName, title, phone, email;

  Future<void> checkIsHasUpdate() async {
    final isUpdate = storage.read(BaseKey.isUpdate) ?? false;
    if (isUpdate) {
      _collectingData();
    } else {
      gotoProfile();
    }
  }

  String vCard() {
    var vcard = Contact();

    if (rawDataContact != null) {
      final name = rawDataContact?['name'];
      // final address = rawDataContact?['address'];

      // name section
      vcard.name.first = name['First Name'] ?? '';
      firstName = vcard.name.first;
      vcard.name.last = name['Last Name'] ?? '';
      lastName = vcard.name.last;

      title = name['Title'] ?? '';
      vcard.organizations = [
        Organization(
          title: title ?? '',
          company: BaseString.companyName,
        )
      ];

      phone = name['Mobile Phone'];
      vcard.phones = [
        Phone(phone != null ? '+62-$phone' : '', label: PhoneLabel.mobile),
        Phone(BaseString.companyPhone, label: PhoneLabel.companyMain),
        Phone(BaseString.companyPhone, label: PhoneLabel.faxWork)
      ];

      email = name['Email'] ?? '';
      vcard.emails = [Email(email ?? '', label: EmailLabel.work)];
      vcard.addresses = [
        Address('',
            street: '${BaseString.street1} ${BaseString.street2}',
            label: AddressLabel.work,
            city: BaseString.city,
            state: BaseString.province,
            postalCode: BaseString.postalCode,
            country: BaseString.country)
      ];
    }

    return vcard.toVCard(withPhoto: false);
  }

  Future _collectingData() async {
    setState(() {
      rawDataContact = Map<String, dynamic>();
      rawDataContact!.addAll({
        'name': storage.read('name'),
        // 'address' : storage.read('address')
      });
    });
  }

  Future<void> gotoProfile() async {
    Get.to(() => ProfilePage(
          firstName: firstName ?? '',
          lastName: lastName ?? '',
          title: title ?? '',
          phone: phone ?? '',
          email: email ?? '',
        ))?.then((value) {
      if (value != null) {
        _collectingData();
      }
    });
  }
}
