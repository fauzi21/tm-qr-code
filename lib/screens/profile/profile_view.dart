import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tm_qrcode/base/base_key.dart';
import 'package:tm_qrcode/base/base_string.dart';
import 'package:tm_qrcode/components/build_default_button.dart';
import 'package:tm_qrcode/components/build_default_textfield.dart';
import 'package:tm_qrcode/components/divider_widget.dart';

class ProfileView extends StatelessWidget {

  final Function onSaved;
  final TextEditingController firstNamController, lastNameController, titleController, phoneController, emailController;

  ProfileView({Key? key,
    required this.onSaved,
    required this.firstNamController,
    required this.lastNameController,
    required this.titleController,
    required this.phoneController,
    required this.emailController
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return ListView(
      key: Key(BaseKey.listViewProfile),
      padding: EdgeInsets.all(12),
      children: [
        ...buildGroup('Name',
          children: [
            BuildDefaultTextfield(
              key: Key(BaseKey.firstName),
              editingController: firstNamController,
              lableText: 'First Name',
              capitalization: TextCapitalization.words,
              margin: EdgeInsets.only(
                bottom: 12
              ),
            ),

            BuildDefaultTextfield(
              key: Key(BaseKey.lastName),
              editingController: lastNameController,
              lableText: 'Last Name',
              capitalization: TextCapitalization.words,
              margin: EdgeInsets.only(
                  bottom: 12
              ),
            ),

            BuildDefaultTextfield(
              key: Key(BaseKey.title),
              editingController: titleController,
              lableText: 'Title',
              capitalization: TextCapitalization.words,
              margin: EdgeInsets.only(
                  bottom: 12
              ),
              isRequired: false,
            ),

            BuildDefaultTextfield(
              key: Key(BaseKey.mobilePhone),
              lableText: 'Mobile Phone',
              editingController: phoneController,
              prefix: '+62- ',
              textInputType: TextInputType.number,
                margin: EdgeInsets.only(
                    bottom: 12
                ),
            ),

            BuildDefaultTextfield(
              key: Key(BaseKey.email),
              editingController: emailController,
              lableText: 'Email',
              autovalidateMode: AutovalidateMode.onUserInteraction,
              margin: EdgeInsets.only(
                  bottom: 12
              ),
              textInputType: TextInputType.emailAddress,
              validator: (value) {
                if (value != null && value.isNotEmpty) {
                  if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@tokiomarine-life.co.id+").hasMatch(value)) {
                    return 'Please enter your email with @tokiomarine-life.co.id';
                  }
                }
              },
            )
          ]
        ),

        Container(
          margin: EdgeInsets.only(
            top: 6
          ),
          child: DividerWidget()
          ,
        ),

        ...buildGroup('Address',
          children: [
            BuildDefaultTextfield(
              value: BaseString.companyName,
              lableText: 'Company',
              isEnabled: false,
              margin: EdgeInsets.only(
                  bottom: 12
              ),
            ),

            BuildDefaultTextfield(
              value: BaseString.companyPhone,
              isEnabled: false,
              lableText: 'Company Phone',
              margin: EdgeInsets.only(
                  bottom: 12
              ),
            ),

            BuildDefaultTextfield(
              value: BaseString.fax,
              isEnabled: false,
              lableText: 'Fax',
              margin: EdgeInsets.only(
                  bottom: 12
              ),
            ),

            BuildDefaultTextfield(
              lableText: 'Street 1',
              value: BaseString.street1,
              isEnabled: false,
            ),

            BuildDefaultTextfield(
              lableText: 'Street 2',
              value: BaseString.street2,
              isEnabled: false,
            ),

            BuildDefaultTextfield(
              lableText: 'City',
              value: BaseString.city,
              isEnabled: false,
            ),

            BuildDefaultTextfield(
              lableText: 'Province',
              value: BaseString.province,
              isEnabled: false,
            ),

            BuildDefaultTextfield(
              lableText: 'Postal Code',
              value: BaseString.postalCode,
              isEnabled: false,
            ),

            BuildDefaultTextfield(
              lableText: 'Country',
              value: BaseString.country,
              isEnabled: false,
            ),
          ]
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: List.generate(2, (index) {

            bool isSaveBtn = index == 1;

            return Container(
              margin: EdgeInsets.only(
                left: 12
              ),
              child: BuildDefaultButton(
                key: Key(isSaveBtn ? BaseKey.btnSave : BaseKey.btnCancel),
                lable: isSaveBtn ? 'Save' : 'Cancel',
                buttonColor: isSaveBtn ? Colors.cyan : null,
                textColor: isSaveBtn ? Colors.white : Colors.grey,
                elevation: 0,
                onPressed: () {
                  if (!isSaveBtn) {
                    Get.back();
                    return;
                  }

                  onSaved();
                },
              ),
            );
          }).toList(),
        )
      ],
    );
  }

  List<Widget> buildGroup(String title, {
    required List<Widget> children
  }) {
    return [
      Container(
        margin: EdgeInsets.only(
          bottom: 12
        ),
        child: Text(title,
          style: TextStyle(
              fontSize: 20,
              color: Colors.black54,
              fontWeight: FontWeight.bold
          ),
        ),
      ),

      ...children
    ];
  }
}
