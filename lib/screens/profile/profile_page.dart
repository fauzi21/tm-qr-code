part of 'profile_state.dart';

class ProfilePage extends StatefulWidget {

  final String firstName, lastName, title, phone, email;

  const ProfilePage({Key? key,
    required this.firstName,
    required this.lastName,
    required this.title,
    required this.phone,
    required this.email
  }) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends ProfileState<ProfilePage> {

  @override
  void initState() {
    firstNameController..text = widget.firstName;
    lastNameController..text = widget.lastName;
    emailController..text = widget.email;
    phoneController..text = widget.phone;
    titleController..text = widget.title;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BuildAppBar('TM QRcode'),
      backgroundColor: Colors.white,
      body: Form(
        key: formKey,
        child: ProfileView(
          firstNamController: firstNameController,
          lastNameController: lastNameController,
          emailController: emailController,
          phoneController: phoneController,
          titleController: titleController,
          onSaved: onSave,
        ),
      ),
    );
  }
}
