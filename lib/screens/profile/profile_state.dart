import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:tm_qrcode/base/base_key.dart';
import 'package:tm_qrcode/base/base_string.dart';
import 'package:tm_qrcode/components/build_appbar.dart';
import 'package:tm_qrcode/screens/profile/profile_view.dart';

part 'profile_page.dart';

abstract class ProfileState<T extends StatefulWidget> extends State<T> {

  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController titleController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  final storage = GetStorage();

  final formKey = GlobalKey<FormState>();

  Future<void> onSave() async {
    final isValid = formKey.currentState?.validate() ?? false;
    if (!isValid) return;

    await storage.write(BaseKey.isUpdate, true);
    await storage.write('name', {
      'First Name': firstNameController.text,
      'Last Name': lastNameController.text,
      'Title': titleController.text,
      // 'Company': BaseString.companyName,
      'Mobile Phone': phoneController.text,
      // 'Company Phone': BaseString.companyPhone,
      // 'Fax': BaseString.fax,
      'Email': emailController.text
    });

    Get.back(result: true);
  }
}