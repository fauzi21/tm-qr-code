import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:tm_qrcode/base/base_color.dart';
import 'package:tm_qrcode/base/base_string.dart';
import 'package:tm_qrcode/screens/main_state.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  final storage = GetStorage();

  @override
  Widget build(BuildContext context) {

    // storage.writeIfNull('address', {
    //   'Company': BaseString.companyName,
    //   'Company Phone': BaseString.companyPhone,
    //   'Fax': BaseString.fax,
    //   'Street 1': BaseString.street1,
    //   'Street 2': BaseString.street2,
    //   'City': BaseString.city,
    //   'Province': BaseString.province,
    //   'Postal Code': BaseString.postalCode,
    //   'Country': BaseString.country
    // });

    storage.writeIfNull('name', {
      'First Name': '',
      'Last Name': '',
      'Title': '',
      'Mobile Phone': '',
      'Email': ''
    });

    return GetMaterialApp(
      title: 'TM QRcode',
      theme: ThemeData(
        primaryColor: mainColor,
        backgroundColor: Colors.white,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MainPage(),
    );
  }
}
