class BaseKey {
  static final String firstName = 'firstName';
  static final String lastName = 'lastName';
  static final String title = 'title';
  static final String mobilePhone = 'mobilePhone';
  static final String email = 'email';
  static final String btnSave = 'btnSave';
  static final String btnCancel = 'btnCancel';
  static final String btnSetting = 'btnSetting';
  static final String listViewProfile = 'listViewProfile';

  static const contact = 'contact';
  static const isUpdate = 'isUpdate';
}