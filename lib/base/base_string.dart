class BaseString {
  static final companyName = 'PT Tokio Marine Life Insurance Indonesia';
  static final companyPhone = '+62-2129751888';
  static final fax = '+62-2129751889';
  static final street1 = 'International Financial Centre Tower 2, Level 33A';
  static final street2 = 'Jl. Jenderal Sudirman Kav. 22-23';
  static final city = 'Jakarta';
  static final province = 'DKI Jakarta';
  static final postalCode = '12920';
  static final country = 'Indonesia';
}