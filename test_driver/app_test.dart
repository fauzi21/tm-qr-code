import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
import 'package:tm_qrcode/base/base_key.dart';

void main() {
  group('TM QR Code', () async {
    FlutterDriver driver = await FlutterDriver.connect();

    // Connect to Flutter Driver before running any test
    // setUpAll(() async {
    //   driver = await FlutterDriver.connect();
    // });

    tearDown(() {
      driver.close();
    });

    // initialize all the field or btn
    final fieldFirstName = find.byValueKey(BaseKey.firstName);
    final fieldLastName = find.byValueKey(BaseKey.lastName);
    final fieldTitle = find.byValueKey(BaseKey.title);
    final fieldMobilePhone = find.byValueKey(BaseKey.mobilePhone);
    final fieldEmail = find.byValueKey(BaseKey.email);

    final btnSave = find.byValueKey(BaseKey.btnSave);
    final btnCancel = find.byValueKey(BaseKey.btnCancel);
    final btnSetting = find.byValueKey(BaseKey.btnSetting);

    final listView = find.byValueKey(BaseKey.listViewProfile);

    Future fillForm() async {
      // fill in the field of profile page
      await driver.waitFor(fieldFirstName);
      await driver.tap(fieldFirstName);
      await driver.enterText('Budi Sumar');

      await driver.waitFor(fieldLastName);
      await driver.tap(fieldLastName);
      await driver.enterText('tono S.M, MM, SS, S.Kom, Phd');

      await driver.waitFor(fieldTitle);
      await driver.tap(fieldTitle);
      await driver.enterText('Software Development');

      await driver.waitFor(fieldMobilePhone);
      await driver.tap(fieldMobilePhone);
      await driver.enterText('19292929');

      await driver.waitFor(fieldEmail);
      await driver.tap(fieldEmail);
      await driver.enterText('Sumar@tokiomarine-life.co.id');

      await driver.scrollUntilVisible(listView, btnCancel, dyScroll: -300.0);
      await delay(3000);

      return null;
    }

    test('Apps Flow', () async {

      await delay(3000);

      await fillForm();

      await driver.waitFor(btnCancel);
      await driver.tap(btnCancel);

      await driver.waitFor(btnSetting);
      await driver.tap(btnSetting);

      await fillForm();

      await driver.waitFor(btnSave);
      await driver.tap(btnSave);

      await delay(5000);

    });
  });
}

Future delay(int miliseonds) {
  return Future.delayed(Duration(
      milliseconds: miliseonds
  ));
}